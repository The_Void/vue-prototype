/* eslint-disable */

const fs = require('fs')
const axios = require('axios')

const API_URL = 'https://aws-staging-api.800loanmart.com'

const JSON_DATA = {},
    YEARS = []

console.log('🦄 Fetch API and create jsonDB')


const storeData = (data, path) => {
    try {
        fs.writeFileSync(path, JSON.stringify(data))
    } catch (err) {
        console.error('🤬 ' + err)
    }
}

const makeApiRequest = (requestMethod, endpoint, data) => {
    let requestObject = {}

    if (requestMethod && requestMethod.toUpperCase() === 'POST' || requestMethod.toUpperCase() === 'GET') {
        requestObject.method = requestMethod.toUpperCase()
    } else {
        console.log('🤬 Error', '"'.concat(requestMethod, '" is not a valid request method.'), requestMethod)
        return Promise.reject(false);
    }

    if (requestObject.method === 'POST') {
        requestObject.data = data
    }

    requestObject.url = API_URL + endpoint;
    requestObject.headers = {
        'Content-Type': 'application/json; charset=utf-8',
        'X-Window-Location': 'http://localhost:8080/',
        'X-Document-Referer': ''
    };
    return axios(requestObject);
}


// storeData({
//     key: 'keyset',
//     value: 'valueset'
// }, './data.json')


const getData = (endpoint, callback, args) => {
    makeApiRequest('GET', endpoint).then(function (response) {
        if (response && response.data) {
            // console.log('🦄 Info', "".concat(endpoint.substring(4), " data retrieved."), response)
            if (typeof callback === 'function')
                callback(response.data, args)
            return response.data
        } else {
            console.log('🤬 Error', 'An error occured submitting this application.', response)
        }
    }).catch(function (errors) {
        console.log('🤬 Error', 'There was an error with the request to '.concat(submissionEndpoint, '.'), errors)

        if (errors) {
            if (errors.response) {
                console.log('🤬 Error', 'Response data of error presented in "infoObject".', errors.response.data)

                if (errors.response.data && errors.response.data.Code) {
                    let errorCode = errors.response.data.Code
                    let errorReasons = errors.response.data.Reason.split('\n')

                    switch (errorCode) {
                        default:
                            console.log('🤬 Error', 'Uncaught Error Code', errorCode)
                    }
                }
            } else if (errors.request) {
                console.log('🤬 Error', 'There is an error in the request.', errors.request)
            } else if (errors.message) {
                console.log('🤬 Error', errors.message, errors)
            } else {
                console.log('🤬 Error', null, errors)
            }
        } else {
            console.log('🤬 Error', 'An unknown error occured', errors)
        }
    })
}


const pushYears = (obj) => {
    obj = obj.reverse()

    obj.forEach((item) => {
        YEARS.push({key: item.Year, value: item.Year})
        JSON_DATA[item.Year] = {}

        getData(`/getvehiclemakes/${item.Year}`, pushMakes, item.Year)
    })

}

const pushMakes = (obj, year) => {
    obj.forEach((item) => {
        JSON_DATA[year][item.MakeDescription] = {id: item.MakeID}
    })

    storeData(JSON_DATA,'./src/api/seeds/api-database.json')
}

let years = getData('/getvehicleyears', pushYears)


// let year = 1981,
//     self = this
//
// while (year < 2021) {
//     let _year = year
//     // console.log('year', _year)
//     this.makeApiRequest('GET', `/getvehiclemakes/${_year}`).then(function (response) {
//         // console.log('response', _year, response.data)
//         self.populateCachedMakesDB(_year, response.data)
//     })
//     year++
// }