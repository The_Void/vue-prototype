import Vue from 'vue';
import VueI18n from 'vue-i18n';
import En from '../locales/en.json';
import Es from '../locales/es.json';

Vue.use(VueI18n);

const messages = {
    'en': En,
    'es': Es
};

export const i18n = new VueI18n({
    locale: 'en',
    fallbackLocale: 'es',
    messages
});