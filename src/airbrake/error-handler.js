import airbrake from './init'

export default function (err, vm, info) {
    if (typeof info.config !== 'undefined')
        info.config.data = ['protected information']
    if (typeof info.arguments !== 'undefined')
        info.arguments = ['protected information']
    airbrake.notify({
        error: err,
        params: {
            info: info
        }
    })
}