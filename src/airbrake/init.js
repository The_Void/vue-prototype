// AirBrakeJS https://airbrake.io
import AirbrakeClient from 'airbrake-js'

export default new AirbrakeClient({
    projectId: 251364,
    projectKey: 'fbca3d216bfcc51c453c0f6170e20f48',
    environment: process.env["NODE_ENV"]
})