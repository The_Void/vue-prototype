import airbrake from './init'
import trackSteps from '@/events/track-steps'

export default function (obj) {
    if (typeof obj.errors.config !== 'undefined')
        obj.errors.data = ['protected information']
    if (typeof obj.errors.arguments !== 'undefined')
        obj.errors.arguments = ['protected information']

    let errorCode = 'noCode',
        errorReasons = ['noReason']


    if (obj.errors && obj.errors.response) {
        if (obj.errors.response.data && obj.errors.response.data.Code) {
            errorCode = obj.errors.response.data.Code
            errorReasons = obj.errors.response.data.Reason.split('\n')
        }
    }


    let error = {
        name: `Api${obj.method}`,
        message: `${obj.endpoint} - ${errorCode}: ${errorReasons[0]}`
    }

    trackSteps('app-tracking', 'error', `${obj.endpoint.replace(/\//gm, '')}-${errorCode}`)

    airbrake.notify({
        error: error,
        context: {
            appDebug: obj
        }
    })
}