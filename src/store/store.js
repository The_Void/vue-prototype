import Vue from 'vue';
import Vuex from 'vuex';
import { residence } from './modules/residence';
import { user } from './modules/user';
import { vehicle } from './modules/vehicle';

Vue.use(Vuex);

// https://vuex.vuejs.org/guide/
// Creating an instance of Vuex
export default new Vuex.Store({
    // https://vuex.vuejs.org/guide/modules.html
    // Defining/declaring modules in store
    modules: {
        residence,
        user,
        vehicle
    }
});