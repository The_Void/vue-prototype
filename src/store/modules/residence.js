// Function that will be used to reset the state
let initialState = () => {
    return { 
        address: '',
        ccb_states: [
            "DE",
            "DC",
            "FL",
            "IL",
            "IN",
            "KS", 
            "KY",
            "MI",
            "MS",
            "OH",
            "OK",
            "OR",
            "SD",
            "TN",
            "TX",
            "WA"           
        ],
        city: '',
        state: '',
        updated_state: '',
        zip_code: '',
    }
}; 
// User module
export const residence = {
    // https://vuex.vuejs.org/guide/modules.html#namespacing
    // Allows module to be more self-contained and/or reuseable
    namespaced: true,
    // https://vuex.vuejs.org/guide/state.html#state
    state: initialState,
    // https://vuex.vuejs.org/guide/mutations.html#mutations
    mutations: {
        UPDATE_ADDRESS(state, payload) {
            state.address = payload;
        },
        UPDATE_CITY(state, payload) {
            state.city = payload;
        },
        UPDATE_PRIMARY_NUMBER(state, payload) {
            state.primary_number = payload;
        },
        UPDATE_SECONDARY_NUMBER(state, payload) {
            state.secondary_number = payload;
        },
        UPDATE_STATE(state, payload) {
            state.state = payload;
        },
        UPDATE_UPDATED_STATE(state, payload) {
            state.updated_state = payload;
        },
        UPDATE_ZIP_CODE(state, payload) {
            state.zip_code = payload;
        },
        RESET_STATE(state) {
            // acquire initial state
            const clonedState = initialState();
            
            Object.keys(clonedState).forEach(key => {
                state[key] = clonedState[key]
            });
        }
    },
}