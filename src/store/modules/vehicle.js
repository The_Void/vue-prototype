// Function that will be used to reset the state
let initialState = () => {
    return { 
        year: '',
        make: '',
        model: '',
        style: '',
        mileage: '',
        ownership: '',
        makes_list: [],
        models_list: [],
        styles_list: [],
    }
}; 
// User module
export const vehicle = {
    // https://vuex.vuejs.org/guide/modules.html#namespacing
    // Allows module to be more self-contained and/or reuseable
    namespaced: true,
    // https://vuex.vuejs.org/guide/state.html#state
    state: initialState,
    // https://vuex.vuejs.org/guide/mutations.html#mutations
    mutations: {
        UPDATE_YEAR(state, payload) {
            state.year = payload;
        },
        UPDATE_MAKE(state, payload) {
            state.make = payload;
        },
        UPDATE_MODEL(state, payload) {
            state.model = payload;
        },
        UPDATE_STYLE(state, payload) {
            state.style = payload;
        },
        UPDATE_MILEAGE(state, payload) {
            state.mileage = payload;
        },
        UPDATE_OWNERSHIP(state, payload) {
            state.ownership = payload;
        },
        UPDATE_MAKES_LIST(state, payload) {
            state.makes_list.push(payload);
        },
        UPDATE_MODELS_LIST(state, payload) {
            state.models_list.push(payload);
        },
        UPDATE_STYLES_LIST(state, payload) {
            state.styles_list.push(payload);
        },
        RESET_STATE(state) {
            // acquire initial state
            const clonedState = initialState();
            
            Object.keys(clonedState).forEach(key => {
                state[key] = clonedState[key]
            });
        }
    },
}