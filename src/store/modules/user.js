// Function that will be used to reset the state
let initialState = () => {
    return { 
        active_military: '',
        callInNumber: '',
        consent_to_share_information: '',
        default_loan_amount: '',
        default_term: '',
        email: '',
        first_name: '',
        last_name: '',
        loan_number: '',
        laon_type: '',
        monthly_income: '',
        primary_number: '',
        secondary_number: '',
        terms_and_conditions: '',
        texts_and_calls: '',
    }
}; 
// User module
export const user = {
    // https://vuex.vuejs.org/guide/modules.html#namespacing
    // Allows module to be more self-contained and/or reuseable
    namespaced: true,
    // https://vuex.vuejs.org/guide/state.html#state
    state: initialState,
    // https://vuex.vuejs.org/guide/mutations.html#mutations
    mutations: {
        UPDATE_ACTIVE_MILITARY(state, payload) {
            state.active_military = payload;
        },
        UPDATE_CALL_IN_NUMBER(state, payload) {
            state.callInNumber = payload;
        },
        UPDATE_DEFAULT_LOAN_AMOUNT(state, payload) {
            state.default_loan_amount = payload;
        },
        UPDATE_DEFAULT_TERM(state, payload) {
            state.default_term = payload;
        },
        UPDATE_CONSENT_TO_SHARE_INFORMATION(state, payload) {
            state.consent_to_share_information = payload;
        },
        UPDATE_EMAIL(state, payload) {
            state.email = payload;
        },
        UPDATE_FIRST_NAME(state, payload) {
            state.first_name = payload;
        },
        UPDATE_LAST_NAME(state, payload) {
            state.last_name = payload;
        },
        UPDATE_LOAN_NUMBER(state, payload) {
            state.loan_number = payload;
        },
        UPDATE_LOAN_TYPE(state, payload) {
            state.loan_type = payload;
        },
        UPDATE_MONTHLY_INCOME(state, payload) {
            state.monthly_income = payload;
        },
        UPDATE_PRIMARY_NUMBER(state, payload) {
            state.primary_number = payload;
        },
        UPDATE_SECONDARY_NUMBER(state, payload) {
            state.secondary_number = payload;
        },
        UPDATE_TERMS_AND_CONDITIONS(state, payload) {
            state.terms_and_conditions = payload;
        },
        UPDATE_TEXTS_AND_CALLS(state, payload) {
            state.texts_and_calls = payload;
        },
        RESET_STATE(state) {
            // acquire initial state
            const clonedState = initialState();
            
            Object.keys(clonedState).forEach(key => {
                state[key] = clonedState[key]
            });
        }
    },
}