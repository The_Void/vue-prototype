import Vue from 'vue';
import App from './App.vue';
import errorHandler from './airbrake/error-handler'
// Router
import router from './router';
// Vuex
import store from './store/store';
// Bootstrap
// Translation plugin
import { i18n } from '@/vuei18n/plugins/i18n.js';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
// Vee-Validate
import './validation/vee-validate';
// The Mask (input masking)
import VueTheMask from 'vue-the-mask';
// Cookie plugin
// https://github.com/cmp-cc/vue-cookies
import VueCookies from 'vue-cookies';
// https://github.com/FortAwesome/vue-fontawesome
import { library } from '@fortawesome/fontawesome-svg-core';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { faArrowCircleLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

Vue.config.productionTip = false;

Vue.config.errorHandler = errorHandler

// https://vuejs.org/v2/api/#Vue-use
Vue.use(VueTheMask);
Vue.use(VueCookies);
// https://vuejs.org/v2/api/#Vue-use
Vue.component('font-awesome-icon', FontAwesomeIcon);

// Font Awesome spinner
library.add(faSpinner, faArrowCircleLeft);

window.app = new Vue({
    router,
    i18n,
    store,
    render: h => h(App),
    data() {
        return {
            realApiUrl: process.env.VUE_APP_API_URL,
        }
    },
    mounted() {
        this.$router.replace('/');
        // this.$i18n.locale = "es";
        // this.locale = "es";
        // console.log(this.$store.state.user)
    },
    computed: {
        apiUrl: {
            get() {
                return this.realApiUrl;
            },
            set(url) {
                this.realApiUrl = url;
            }
        }
    },
}).$mount('#app');
