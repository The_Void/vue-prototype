import Vue from 'vue'
import Router from 'vue-router'
import Mobile_Touch_Template from './views/Mobile_Touch_Template.vue'

Vue.use(Router)

export default new Router({
  mode: 'abstract',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'mobile_touch_template',
      component: Mobile_Touch_Template
    },
  ]
})
