import Vue from "vue";
import { extend, localize } from "vee-validate";
import { 
    required, 
    email, 
    min, 
    alpha_spaces, 
    max, 
    regex,
    min_value,
    integer } from "vee-validate/dist/rules";
import es from "vee-validate/dist/locale/es.json";
import en from "vee-validate/dist/locale/en.json";

// Install rules.
extend("required", required);
extend("email", email);
extend("min", min);
extend("max", max);
extend("regex", regex);
extend("alpha_spaces", alpha_spaces);
extend("min_value", min_value);
extend("integer", integer);

// Install English and Spanish localizations.
localize({
    en: {
        messages: en.messages,
        names: {
            email: "E-mail Address",
            password: "Password",
            first_name: "First name",
            last_name: "Last name",
            primary_number: "Mobile number",
            secondary_number: "Home number",
            state: "State",
            city: "City",
            zip_code: "Zip Code",
            active_military: "This field",
            terms_and_conditions: "This field",
            texts_and_calls: "This field",
            consent_to_share_information: "This field",
        },
        fields: {
            email: {
                required: "{_field_} is required.",
                email: "{_field_} must be in a proper format."
            },
            first_name: {
                required: "{_field_} is required.",
                alpha_spaces: "{_field_} can only consist of letters and spaces."
            },
            last_name: {
                required: "{_field_} is required.",
                alpha_spaces: "{_field_} can only consist of letters and spaces."
            },
            primary_number: {
                required: "{_field_} is required.",
                regex: "{_field_} is invalid.",
                min: "{_field_} must consist of at least 10 characters.",
                max: "{_field_} must consist of less than 11 characters."
            },
            secondary_number: {
                required: "{_field_} is required.",
                regex: "{_field_} is invalid.",
                min: "{_field_} must consist of at least 10 characters.",
                max: "{_field_} must consist of less than 11 characters."
            },
            state: {
                required: "{_field_} is required.",
            },
            city: {
                required: "{_field_} is required.",
            },
            zip_code: {
                required: "{_field_} is not specified.",
                integer: "{_field_} can only consist of numbers.",
                min_value: "{_field_} is invalid.",
                max_value: "{_field_} is incorrect.",
                min: "{_field_} must consist of 5 numbers.",
                max: "{_field_} must consist of 5 numbers."
            },
            active_military: {
                required: "We cannot offer you a loan if you are a regular or reserve member of the US military",
            },
            terms_and_conditions: {
                required: "{_field_} is required.",
            },
            texts_and_calls: {

            },
            consent_to_share_information: {

            },
        }
    },
    es: {
        messages: es.messages,
        names: {
            email: "correo electrónico",
            password: "Password",
            first_name: "Nombre",
            last_name: "Apellido",
            primary_number: "Número de teléfono",
            secondary_number: "Número de teléfono",
            state: "Estado",
            city: "ciudad",
            zip_code: "Código Postal",
            active_military: "This field",
            terms_and_conditions: "This field",
            texts_and_calls: "This field",
            consent_to_share_information: "This field",
        },
        fields: {
            email: {
                required: "La dirección de {_field_} no puede estar en blanco.",
                email: "Correo electrónico invalido."
            },
            first_name: {
                required: "{_field_} (no puede quedar en blanco).",
                alpha_spaces: "El nombre solo puede consistir en letras y espacios."
            },
            last_name: {
                required: "{_field_} (no puede quedar en blanco).",
                alpha_spaces: "El apellido solo puede consistir en letras y espacios."
            },
            primary_number: {
                required: "Número de teléfono (no puede quedar en blanco).",
                regex: "El número de teléfono no es válido.",
                min: "El número de teléfono debe constar de al menos 10 caracteres.",
                max: "Número de teléfono debe constar de menos de 11 caracteres."
            },
            secondary_number: {
                required: "Número de teléfono (no puede quedar en blanco).",
                regex: "El número de teléfono no es válido.",
                min: "El número de teléfono debe constar de al menos 10 caracteres.",
                max: "Número de teléfono debe constar de menos de 11 caracteres."
            },
            state: {
                required: "{_field_} (no puede quedar en blanco).",
            },
            city: {
                required: "La {__field__} solo puede consistir en letras y espacios.",
            },
            zip_code: {
                required: "{_field_} (no puede quedar en blanco).",
                integer: "El código postal solo puede consistir en números.",
                min_value: "El código postal no es válido.",
                max_value: "El código postal no es válido.",
                min: "El código postal debe constar de 3 a 5 números.",
                max: "El código postal debe constar de 3 a 5 números."
            },
            active_military: {
                required: "Al marcar esta casilla, confirmas que no eres miembro del ejército, armada, Infantería de Marina, fuerza aérea o Guardacostas, regular o reserva, sirviendo en el servicio activo, ni cónyuge, un hijo o un dependiente de tal miembro.",
            },
            terms_and_conditions: {
                required: "Debe aceptar nuestros términos y condiciones para continuar.",
            },
            texts_and_calls: {

            },
            consent_to_share_information: {

            },
        }
    }
});

let LOCALE = "en";

// A simple get/set interface to manage our locale in components.
// This is not reactive, so don't create any computed properties/watchers off it.
Object.defineProperty(Vue.prototype, "locale", {
  get() {
    return LOCALE;
  },
  set(val) {
    LOCALE = val;
    localize(val);
  }
});
