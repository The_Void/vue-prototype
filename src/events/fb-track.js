import debugLog from '@/debug/log'
import debugInfo from '@/debug/info'
import debugError from '@/debug/error'

export default function (value) {
    let track = value ? ['track', value]
                        : ['track', 'default']

    debugLog('FB Tracking :', track)

    if (process.env["NODE_ENV"] !== "development" && typeof window.fbq === 'function') {
        try {
            fbq(track);
        } catch (except) {
            debugError('FB Tracking Error: ', except);
        }
    }
}