import debugLog from '@/debug/log'
import debugInfo from '@/debug/info'
import debugError from '@/debug/error'

/**
 * Events with Google Analytics through Tealium
 *
 * @link https://developers.google.com/analytics/devguides/collection/analyticsjs/sending-hits
 * @link https://community.tealiumiq.com/t5/Tags/Google-Universal-Analytics-Event-Tracking/ta-p/14082
 * @since 08/08/2019
 */

export default function (target, action, label) {

    debugLog('GA event:', {'target': target, 'action': action, 'label': label})

    if (process.env["NODE_ENV"] !== "development") {

        if (typeof window.utag === "object") { // Tealium
            try {
                window.utag.link({
                    ga_events: [{
                        eventCategory: target,
                        eventAction: action,
                        eventLabel: label,
                    }]
                })
            } catch (except) {
                debugError('Oops something wrong...', except)
            }
        } else if (typeof window.dataLayer === "object") { // Google Tag
            try {
                window.dataLayer('event', action, {
                    'event_category': target,
                    'event_label': label
                })
            } catch (except) {
                debugError('Oops something wrong...', except)
            }
        } else if (typeof window.ga === "function") { // Google Analytics
            try {
                window.ga('send', {
                    hitType: 'event',
                    eventCategory: target,
                    eventAction: action,
                    eventLabel: label
                })
            } catch (except) {
                debugError('Oops something wrong...', except)
            }
        } else { // do nothing
            debugInfo('GA event: no one listener founded')
        }

    }
}