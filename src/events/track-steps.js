import debugLog from '@/debug/log'
import debugInfo from '@/debug/info'
import debugError from '@/debug/error'
import gaEvent from './google'


import { i18n } from '@/vuei18n/plugins/i18n.js';


export default function (target, action, label) {
    let prefix = i18n.locale === 'es' ? 'es-' : ''

    let is_callout = ['click-user-callout'].indexOf(label) + 1

    let is_exception = ['error', 'modal', 'redirect'].indexOf(action) + 1

    if (typeof window.gaEventsPass === 'undefined')
        window.gaEventsPass = {}

    if (is_callout || is_exception || typeof window.gaEventsPass[label] === 'undefined') {

        debugLog('trackSteps:', label)

        window.gaEventsPass[label] = 1

        target = prefix + target

        gaEvent(target, action, label)

    } else {

        debugLog('trackSteps:', label + ' - already tracked')

    }

}