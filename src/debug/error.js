export default function (...args) {
    if (process.env["NODE_ENV"] !== "production") {
        console.error('✉️LM:', ...args)
    }
}